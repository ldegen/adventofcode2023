#!/bin/bash

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
mkdir -p build
cmake \
  -DCMAKE_BUILD_TYPE=Debug\
  -DCMAKE_EXPORT_COMPILE_COMMANDS=1\
  -B build\
  -G Ninja
ninja -C build
